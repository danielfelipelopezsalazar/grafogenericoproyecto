/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.modelo;

import grafociudades.excepciones.GrafoExcepcion;
import java.io.Serializable;

/**
 * etodo que permite verificar en el GrafoNoDirigido cual es el origen y su destino
 * @author carloaiza
 */
public class GrafoNoDirigido extends GrafoAbstract 
        implements Serializable{
    
    /**
     * Metodo que permite verificar en el GrafoNoDirigido cual es el origen y su destino. Donde un tiene
     * una condicional For en la cual tiene un if dentro suyo en el cual verifica si el origen y el destino
     * en ambos sentido ya existe entre los vertices. Finaliza con un excepcion en la cual informa
     * que ya existe una comunicacion entre los vertices a los cuales se estan intentado conectar
     * @param origen : Origen de donde incia la arista
     * @param destino : Destino al cual llega la arista
     * @throws GrafoExcepcion : Excepcion que puede existir si ya hay conexion entre dos vertices
     */
     @Override
     public void verificarArista(int origen, int destino) throws GrafoExcepcion
    {   
        for(Arista ar: this.getAristas())
        {
            if( (ar.getOrigen()==origen && ar.getDestino()==destino)
                   || (ar.getDestino()==origen && ar.getOrigen()==destino))
            {
                throw new GrafoExcepcion("Ya existe comunicación entre los dos vertices");
            }    
            
            
        }
    }
     
     
}
