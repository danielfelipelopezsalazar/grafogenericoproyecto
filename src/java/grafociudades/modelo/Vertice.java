/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.modelo;

import java.io.Serializable;

/**
 * Clase que almacena la informacion de la clase dato
 * @author carloaiza
 */
public class Vertice implements Serializable{
    private int codigo;
    private Dato dato;
    /**
     * Constructor en cual se pasa la informacion que se le asigna al vertice adicionado, donde
     * se garantiza que el codigo no se repita
     * @param codigo : Codigo identificador del vertice donde se identificara cada uno de los vertices
     * adicionados
     * @param dato : Informacion de tipo Dato, el cual almacena la informacion que tiene la clase
     * Dato.
     */
    public Vertice(int codigo, Dato dato) {
        this.codigo = codigo;
        this.dato = dato;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Dato getDato() {
        return dato;
    }

    public void setDato(Dato dato) {
        this.dato = dato;
    }

    @Override
    public String toString() {
        return codigo + " " + dato;
    }
    
    
    
    
    
    
}
