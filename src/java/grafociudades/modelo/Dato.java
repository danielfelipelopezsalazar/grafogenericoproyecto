/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.modelo;

import java.io.Serializable;
    
/**
 * Clase que permite almacenar los datos de los vertices que se adicionan
 * @author carloaiza
 */
public class Dato implements Serializable{
    private String nombre;
    private int posx;
    private int posy;
        
    public Dato() {
    }
    /**
     * Constructor que permite enviarle y almacenar los datos al vertice al cual fue adicionado
     * @param nombre : Nombre que se le ha asignado al nuevo vertice
     * @param posx : Posicion en "x" en el plano cartesiano que se le asigna al vertice 
     * @param posy : Posicion en "y" en el plano cartesiano que se le asigna al vertice
     */
    public Dato(String nombre, int posx, int posy) {
        this.nombre = nombre;
        this.posx = posx;
        this.posy = posy;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    @Override
    public String toString() {
        return nombre;
    }

    
}