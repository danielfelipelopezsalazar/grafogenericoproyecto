/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.modelo;

import grafociudades.excepciones.GrafoExcepcion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase el cual nos permite realizar adicionar o remover de aristas y vertices,
 * obtener un vertice por un codigo en especifico.
 * @author carloaiza
 */
public abstract class GrafoAbstract implements Serializable{
    private List<Vertice> vertices;
    private List<Arista> aristas;
    /**
     * Creacion de ArrayList donde se almacenaran los direfentes vertices con sus aristas
     */
    public GrafoAbstract() {
        vertices= new ArrayList<>();
        aristas = new ArrayList<>();
    }
    
    

    public List<Vertice> getVertices() {
        return vertices;
    }

    public void setVertices(List<Vertice> vertices) {
        this.vertices = vertices;
    }

    public List<Arista> getAristas() {
        return aristas;
    }

    public void setAristas(List<Arista> aristas) {
        this.aristas = aristas;
    }
    
    
    /**
     * Metodo donde se adiciona un nuevo vertice al grafo.
     * @param vertice : Atributo que se pasa al metodo "adicionarVertice" que trae los
     * parametros obtenidos en la clase Vertice. Que irá almacenado en la lista anteriormente 
     * creada
     */
    public void adicionarVertice(Vertice vertice) 
    {
        /**
         * Metodo encargado de adicionar un nuevo vertice
         */
        vertices.add(vertice);
    }
    /**
     * Metodo donde permite eliminar un vertice por su codigo. Utilizando un condicional
     * for con un if dentro de el, que se encarga de verificar los codigos de los vertices 
     * que estan almacenados con el codigo del vertices seleccionado
     * @param codigo  me recive el codigo del vertice para su respectiva eliminacion
     */
    public void removerVertice(int codigo)
    {
        // List<Vertice> tem = new ArrayList<>();
        /**
         * Condicional que recorre la lista para buscar el codigo seleccionado a eliminar
         */
        for (Vertice ver: vertices)
        {   
            /**
             * Condicional que permite validar si el codigo que se ha seleccionado para eliminar es
             * el mismo al que codigo de la lista del vertice
             */
            if (ver.getCodigo() == codigo)
            {
                // tem.add(ver);
                /**
                 * Metodo encargado de eliminar el vertice que anteriormente se ha buscado por 
                 * codigo de vertice
                 */
                vertices.remove(ver);
            }            
        }
        // vertices = tem;
    }
    /**
     * Metodo que permite agregar una nueva arista en el grafo para conectar dos vertices
     * @param arista : Parametro que almacena los datos de la clase Arista.
     */
    public void adicionarArista(Arista arista) 
    {
        /**
         * Metodo encargado de adicionar una nueva arista
         */
        aristas.add(arista);
    }
    /**
     * Metodo encargado de eliminar una arista entre dos vertices. Con un Condicional
     * for validando los codigos de los vertices que se desean conectar. 
     * @param origen : Codigo que seleccionado para la remocion de arista
     * @param destino : Codigo donde llego la arista desde el origen que se va eliminar 
     */
    public void removerArista(int origen, int destino) 
    {
        /**
         * Creacion de una lista temporal donde irá almacenado el recorrido de lista
         * para la verificacion y eliminacion de la arista a eliminar
         */
        List<Arista> tem= new ArrayList<>();
        /**
         * Condicional For para recorrido de la lista para ubicar arista a eliminar
         */
        for(Arista ar: aristas)
        {
            /**
             * Condicional if para validacion que el origen y destino sean los de la arista
             * seleccionada
             */
            if(!(ar.getOrigen()==origen && ar.getDestino()==destino))
            {
                /**
                 * adicion a la lista temporal la arista con el origen y destino seleccionado
                 */
                tem.add(ar);
            }
        }
        aristas = tem;
        
    }
    
   /**
    * Metodo abstracto que verifica la arista entre dos grafos
    * @param origen : Origen del vertice que se va validar 
    * @param destino : Destino al cual vertice llega para realizarse su validacion
    * @throws GrafoExcepcion : Excepciones por si algo va mal con el origen y el destino 
    * dadas en la clase de GrafoExcepcion
    */ 
   public abstract void verificarArista(int origen, int destino) throws GrafoExcepcion;
    
   /**
    * Metodo en el cual se va buscar un vertice por un codigo en especifico lo va mostrar.
    * El cual tiene un condicional for que recorre todos los vertices para obtener el vertice
    * por el codigo pasado. donde al final retornara el vertice o null en caso de que no 
    * exista
    * @param codigo : Codigo utilizado para buscar el vertice en la lista de vertices
    * @return : Muestra el vertice que se ha buscado o Null si no se ha encontrado vertice 
    * ( no existe en lista )
    */
   public Vertice obtenerVerticexCodigo(int codigo)
   {
       /**
        * Condicional For encargado de recorrer la lista de vertice para encontrar el 
        * elemento buscado.
        */
       for(Vertice vert: vertices)
       {
           /**
            * Condicional If el cual se encarga de comprobar que el codigo del vertice en la lista
            * concida con el codigo que se ha pasado para realizar la busqueda
            */
           if(vert.getCodigo()==codigo)
           {
               /**
                * Retorna el vertice en la lista buscado por codigo
                */
               return vert;
               
           }    
       }    
       /**
        * retorna null ya que no encontro el vertice buscado por codigo porque no existe en la lista
        * recorrida
        */
       return null;
   }
    
}
