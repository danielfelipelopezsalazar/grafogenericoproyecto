/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.modelo;

import grafociudades.excepciones.GrafoExcepcion;
import java.io.Serializable;

/**
 * Clase que se comporta y hereda de la clase GrafoAbstract
 * @author carloaiza
 */
public class GrafoDirigido extends GrafoAbstract implements Serializable{
    /**
     * Metodo que permite verificar en el GrafoDirigido cual es el origen y destino
     * para validar si dos vertices ya tienen una arista seleccionada, mediante un
     * condicional For con un If dentro de el, en el cual compara el origen y el destino de la arista en
     * la cual esta seleccionada con las que han pasado. Al finalizar arroja un throws de la clase 
     * GrafoException informando que ya existe una arista para lo vertices que selecionados.
     * @param origen : Origen de donde incia la arista
     * @param destino : Destino al cual llega la arista
     * @throws GrafoExcepcion : Excepcion que puede existir si ya hay conexion entre dos vertices
     */
    @Override
    public void verificarArista(int origen, int destino) throws GrafoExcepcion {
        /**
         * Condicional For que reporre las aristar buscando el origen y destino dado
         */
        for(Arista ar: this.getAristas())
        {
            /**
             * Condicional If que valida que el origen y destino sea el que se ha pasado
             */
            if(ar.getOrigen()==origen && ar.getDestino()==destino)
            {
                /**
                 * Excepcion que se lanza cuando ya hay conexion entre dos vertices
                 */
                throw new GrafoExcepcion("Ya existe comunicación entre los dos vertices");
            }    
            
            
        }
    }
    
}
