/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.controlador;

import grafociudades.excepciones.GrafoExcepcion;
import grafociudades.modelo.Arista;
import grafociudades.modelo.Dato;
import grafociudades.modelo.GrafoAbstract;
import grafociudades.modelo.GrafoNoDirigido;
import grafociudades.modelo.Vertice;
import grafociudades.utilidad.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Singleton;
import org.primefaces.PrimeFaces;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.diagram.ConnectEvent;
import org.primefaces.event.diagram.ConnectionChangeEvent;
import org.primefaces.event.diagram.DisconnectEvent;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.endpoint.RectangleEndPoint;
import org.primefaces.model.diagram.overlay.LabelOverlay;

/**
 * Clase que se encarga de ejecutar las operacion entre el modelo y la vista.
 * que tiene que ver con el grafo
 * @author carloaiza
 */
@Named(value = "controladorGrafo")
@Singleton
public class ControladorGrafo implements Serializable {

    private GrafoNoDirigido grafoND;
    private DefaultDiagramModel model;
    private Dato dato = new Dato();
    private boolean suspendEvent;
    private List<Vertice> rutaCorta;

    private int codigoInicio = 0;
    private int codigoFinal = 0;

    /**
     * Creates a new instance of ControladorGrafo
     */
    public ControladorGrafo() {
    }

    public Dato getDato() {
        return dato;
    }

    public void setDato(Dato dato) {
        this.dato = dato;
    }

    public GrafoNoDirigido getGrafoND() {
        return grafoND;
    }

    public void setGrafoND(GrafoNoDirigido grafoND) {
        this.grafoND = grafoND;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public List<Vertice> getRutaCorta() {
        return rutaCorta;
    }

    public void setRutaCorta(List<Vertice> rutaCorta) {
        this.rutaCorta = rutaCorta;
    }

    public int getCodigoInicio() {
        return codigoInicio;
    }

    public void setCodigoInicio(int codigoInicio) {
        this.codigoInicio = codigoInicio;
    }

    public int getCodigoFinal() {
        return codigoFinal;
    }

    public void setCodigoFinal(int codigoFinal) {
        this.codigoFinal = codigoFinal;
    }
    /**
     * Metodo encargado de inicializar un grafo no dirigido, con vertices y aristas
     * por defecto al crear el grafo cuando se ejecuta el grafo.
     */
    @PostConstruct
    public void inicializar() {
        grafoND = new GrafoNoDirigido();
        /*
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Ciudad("Manizales", 500000, 140000,4,4)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Ciudad("Chinchiná", 90000, 70000,10,10)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Ciudad("Santa Rosa", 110000, 85000,20,10)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Ciudad("Pereira", 190000, 170000,30,4)));
         */

        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("A", 500000, 140000)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("B", 90000, 70000)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("C", 110000, 85000)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("D", 190000, 170000)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("E", 190000, 170000)));
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("F", 190000, 170000)));

        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("G", 190000, 170000)));

        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                new Dato("H", 190000, 170000)));

        //llenado de aristas
        grafoND.getAristas().add(new Arista(1, 2, 6));
        grafoND.getAristas().add(new Arista(1, 4, 5));
        grafoND.getAristas().add(new Arista(1, 7, 8));
        grafoND.getAristas().add(new Arista(2, 3, 7));
        grafoND.getAristas().add(new Arista(2, 4, 1));
        grafoND.getAristas().add(new Arista(3, 4, 4));
        grafoND.getAristas().add(new Arista(3, 5, 11));
        grafoND.getAristas().add(new Arista(3, 6, 2));
        grafoND.getAristas().add(new Arista(4, 5, 6));
        grafoND.getAristas().add(new Arista(4, 8, 3));
        grafoND.getAristas().add(new Arista(5, 6, 2));
        grafoND.getAristas().add(new Arista(5, 7, 10));
        grafoND.getAristas().add(new Arista(6, 8, 4));
        grafoND.getAristas().add(new Arista(7, 8, 2));

        pintarGrafo(grafoND, model);

        //Dijkstra dijstra = new Dijkstra(grafoND, grafoND.getVertices().get(0), grafoND.getVertices().get(6));
        //dijstra.calcularRutaMasCorta();
    }
    /**
     * Metodo donde se trazan las lineas en la vista de las aristas, con los respectivos tamaños
     * de las lineas y color, permite agregar un nuevo dato,
     * @param grafo 
     * @param modelo 
     */
    private void pintarGrafo(GrafoAbstract grafo, DefaultDiagramModel modelo) {
        //int x = 4;
        //int y = 4;
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);

        // model.getDefaultConnectionOverlays().add(new ArrowOverlay(20, 20, 1, 1));
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:3}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");

        model.setDefaultConnector(connector);

        for (Vertice vert : grafo.getVertices()) {
            Element element = new Element(vert);

            element.setX(String.valueOf(vert.getDato().getPosx()) + "em");
            element.setY(String.valueOf(vert.getDato().getPosy()) + "em");
            element.setId(String.valueOf(vert.getCodigo()));
            //x = x + 10;

            EndPoint endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
            endPointSource.setSource(true);
            //endPointSource.setTarget(true);
            element.addEndPoint(endPointSource);

            EndPoint endPoint = createDotEndPoint(EndPointAnchor.TOP);
            endPoint.setTarget(true);
            element.addEndPoint(endPoint);
            element.setDraggable(false);
            model.addElement(element);
        }

        //Pintar aristas
        for (Arista ar : grafoND.getAristas()) {
            //Encuentro origen
            for (Element el : model.getElements()) {
                if (el.getId().compareTo(String.valueOf(ar.getOrigen())) == 0) {
                    for (Element elDes : model.getElements()) {
                        if (elDes.getId().compareTo(String.valueOf(ar.getDestino())) == 0) {
                            Connection conn = new Connection(el.getEndPoints().get(0), elDes.getEndPoints().get(1));
                            conn.getOverlays().add(new LabelOverlay(String.valueOf(ar.getPeso()), "flow-label", 0.5));

                            model.connect(conn);
                            break;
                        }
                    }
                }
            }
        }

    }
    /**
     * Metodo que permite agregar un nuevo dato
     */
    public void adicionarDato() {
        grafoND.adicionarVertice(new Vertice(grafoND.getVertices().size() + 1,
                dato));

        JsfUtil.addSuccessMessage("Dato Adicionado");

        dato = new Dato();
        pintarGrafo(grafoND, model);
    }

    public void limpiarDato() {
        dato = new Dato();
    }
    /**
     * Creacion de los rectangulos para pintar el grafo en la web
     * @param anchor
     * @return 
     */
    private EndPoint createRectangleEndPoint(EndPointAnchor anchor) {
        RectangleEndPoint endPoint = new RectangleEndPoint(anchor);
        endPoint.setScope("ciudad");
        endPoint.setSource(true);
        endPoint.setStyle("{fillStyle:'#98AFC7'}");
        endPoint.setHoverStyle("{fillStyle:'#5C738B'}");

        return endPoint;
    }
    /**
     * Metodo que permite realizar la conexion de dos aristas del origen al fin trazando una linea
     * @param event  recibe el evento que permite realizar el trazadado de la linea
     */

    public void onConnect(ConnectEvent event) {
        if (!suspendEvent) {

            int origen = Integer.parseInt(event.getSourceElement().getId());
            int destino = Integer.parseInt(event.getTargetElement().getId());
            FacesMessage msg = null;
            try {
                grafoND.verificarArista(origen, destino);
                grafoND.adicionarArista(new Arista(origen, destino, 1));
                msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Conectado",
                        "Desde " + event.getSourceElement().getData() + " hacia " + event.getTargetElement().getData());

            } catch (GrafoExcepcion ex) {
                msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");

            }
            pintarGrafo(grafoND, model);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            PrimeFaces.current().ajax().update("frmGrafo");
            PrimeFaces.current().ajax().update("frmCiudad");
        } else {
            suspendEvent = false;
        }
    }
    /**
     * Metodo que permite quitar la conexion de aristas.
     * @param event recibe el evento que permite quitar la conexion de las aristas
     */

    public void onDisconnect(DisconnectEvent event) {

        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Desconectado",
                "Desde " + event.getSourceElement().getData() + " hacia " + event.getTargetElement().getData());

        int origen = Integer.parseInt(event.getSourceElement().getId());
        int destino = Integer.parseInt(event.getTargetElement().getId());
        grafoND.removerArista(origen, destino);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        PrimeFaces.current().ajax().update("frmGrafo");
        PrimeFaces.current().ajax().update("frmCiudad");
    }
    /**
     * Metodo que permite modificar la conexion de dos aristas
     * @param event recive el evento que modifica la conexion de las dos aristas
     */
    
    public void onConnectionChange(ConnectionChangeEvent event) {
        int origenAnt = Integer.parseInt(event.getOriginalSourceElement().getId());
        int destinoAnt = Integer.parseInt(event.getOriginalTargetElement().getId());

        int origen = Integer.parseInt(event.getNewSourceElement().getId());
        int destino = Integer.parseInt(event.getNewTargetElement().getId());
        FacesMessage msg = null;
        try {
            grafoND.removerArista(origenAnt, destinoAnt);
            grafoND.verificarArista(origen, destino);
            grafoND.adicionarArista(new Arista(origen, destino, 1));
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Connección Modificada",
                    "Origen inicial: " + event.getOriginalSourceElement().getData()
                    + ", Nuevo Origen: " + event.getNewSourceElement().getData()
                    + ",Destino inicial: " + event.getOriginalTargetElement().getData()
                    + ", Nuevo Destino: " + event.getNewTargetElement().getData());

        } catch (GrafoExcepcion ex) {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            pintarGrafo(grafoND, model);
        }

        FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().update("frmGrafo");
        PrimeFaces.current().ajax().update("frmCiudad");
        suspendEvent = true;
    }
    /**
     * Metodo que permite crear los puntos de union de un dato.
     * @param anchor
     * @return endPoint
     */
    private EndPoint createDotEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setScope("ciudad");
        endPoint.setTarget(true);
        endPoint.setStyle("{fillStyle:'#98AFC7'}");
        endPoint.setHoverStyle("{fillStyle:'#5C738B'}");

        return endPoint;
    }
    /**
     *Metodo que permite modificar una arista en la intarfaz graficas
     * @param event recive el evento que permite obtener los parametros para realizar la modificacion de la arista
     */

    public void onRowEdit(RowEditEvent event) {
        Arista ar = ((Arista) event.getObject());

        FacesMessage msg = new FacesMessage("Arista Modificada", ar.toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        pintarGrafo(grafoND, model);
        PrimeFaces.current().ajax().update("frmGrafo");

    }
    /**
     * Metodo realiza la cancelacion de edicion de arista.
     * @param event recive el evento que permite la cancelacion de una arista
     */

    public void onRowCancel(RowEditEvent event) {
        Arista ar = ((Arista) event.getObject());
        FacesMessage msg = new FacesMessage("Edición  Cancelada", ar.toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().update("frmGrafo");

    }
    
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        PrimeFaces.current().ajax().update("frmGrafo");
    }
    /**
     * Metodo que permite realizar el llamado para calcular la ruta mas corta en el grafo por medio de un origen y final capturado desde la intefaz grafica
     */

    public void calcularRutaCorta() {
        if (codigoFinal != codigoInicio) {
            Dijkstra dijstra = new Dijkstra(grafoND,
                    grafoND.obtenerVerticexCodigo(codigoInicio), grafoND.obtenerVerticexCodigo(codigoFinal));

            rutaCorta = dijstra.calcularRutaMasCorta();

        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Origen y Destino no pueden ser iguales", "Origen y Destino no pueden ser iguales");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            PrimeFaces.current().ajax().update("grwErrores");
        }
    }
}
