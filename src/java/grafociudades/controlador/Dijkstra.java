/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.controlador;

import grafociudades.modelo.Arista;
import grafociudades.modelo.GrafoNoDirigido;
import grafociudades.modelo.Vertice;
import grafociudades.modelo.VerticeDijkstra;
import grafociudades.utilidad.JsfUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que encapsula las propiedades y los metodos necesarios para trabajar con Dijkstra
 * @author carloaiza
 */
public class Dijkstra {

    private GrafoNoDirigido grafo;
    private Vertice nodoInicio;
    private Vertice nodoFinal;
    /**
     * Metodo constructor que recibe los parametros para instancializar el Dijkstra
     * @param grafo me recive por parametro los metodos del grafo
     * @param nodoInicio me toma los datos del nodo raiz
     * @param nodoFinal a donde apunta la conexion del nodo
     */

    public Dijkstra(GrafoNoDirigido grafo, Vertice nodoInicio, Vertice nodoFinal) {
        this.grafo = grafo;
        this.nodoInicio = nodoInicio;
        this.nodoFinal = nodoFinal;
    }
    /**
     * Metodo permite calcular la ruta mas corta por medio del algoritmo de Dijkstra
     * @return Lista de vertices
     */
  public List<Vertice> calcularRutaMasCorta() {
        List<Vertice> ruta = new ArrayList<>();
        /**
         * inicio es el vertice desde el cual vamos a empezar para hallar la
         * ruta
         */
        VerticeDijkstra inicio = new VerticeDijkstra();
        inicio.setPesoAcumulado(0);
        inicio.setVerticeAntecesor(null);
        inicio.setUsado(true);
        inicio.setVertice(nodoInicio);
        /**
         * listadoVerticesUsados es una lista que me esta almacenando los
         * vertices visitados usados y los visitados no usados
         */
        List<VerticeDijkstra> listadoVerticesUsados = new ArrayList<>();
        listadoVerticesUsados.add(inicio);
        /// se hace dijkstra hasta que todos los vertices estén marcados
        VerticeDijkstra antecesor = inicio;
        inicio.llenarAdyacenciasVertice(grafo, listadoVerticesUsados);
        /**
         * ciclo para verificar que existan aydacencias
         */
        if (!inicio.getListadoAdyacencias().isEmpty()) {

            /**
             * conVertusados es un atributo local para contar el numero de
             * vertices usados
             */
            int contVertusados = 1;
            /**
             * ciclo que ejecuta dijkstra hasta que todos los vertices se
             * visiten y marquen como usados
             */
            while (contVertusados < grafo.getVertices().size()) {
                /**
                 * se llenan las adyacencias del antecesor que no estén marcadas
                 */
                antecesor.llenarAdyacenciasVertice(grafo, listadoVerticesUsados);
                VerticeDijkstra menor = null;
                /**
                 * ciclo que permite verificar si existen adyacencias para poder
                 * obtener la adyacencia menor
                 */
                if (antecesor.getListadoAdyacencias().size() > 0) {
                    menor = antecesor.obtenerAdyacenciaMenorPeso();
                    menor.setUsado(true);
                    /**
                     * este "else" significa que hay un bloqueo en el grafo
                     */
                } else {
                    contVertusados = contarVerticesUsados(listadoVerticesUsados);
                    /**
                     * ciclo que me da la condicion de que si el numero de
                     * vertices usados es igual al numero de vertices usados de
                     * la lista me va a recorrer el grafo y va a obtener el
                     * menos de estos
                     */
                    if (contVertusados == listadoVerticesUsados.size()) {
                        for (int i = 0; i < grafo.getVertices().size(); i++) {
                            menor = obtenerVerticeNoUsado(grafo.getVertices().get(i), listadoVerticesUsados);
                            if (menor != null) {
                                listadoVerticesUsados.add(menor);
                                break;
                            }

                        }

                    } else {
                        /**
                         * este ciclo me recorre lo vertices usados visitados y
                         * los visitados no usados y va a seleccionar un vertice
                         * que este visitado pero no usado para dejarlo como
                         * menor
                         */
                        for (int i = 0; i < listadoVerticesUsados.size(); i++) {
                            if (!listadoVerticesUsados.get(i).isUsado()) {
                                menor = listadoVerticesUsados.get(i);
                                menor.setUsado(true);
                                listadoVerticesUsados.add(menor);
                                contVertusados = contarVerticesUsados(listadoVerticesUsados);
                                break;
                            }

                        }
                    }

                }
//            if (menor == null) {
////                for (int i = 0; i < listadoVerticesUsados.size(); i++) {
////                    if (!listadoVerticesUsados.get(i).isUsado()) {
////                        menor = listadoVerticesUsados.get(i);
////
////                    }
////                }
//                menor = salto(listadoVerticesUsados, grafo.getVertices());
//                listadoVerticesUsados.add(menor);
//            }

                //.........
                listadoVerticesUsados.addAll(this.obtenerAdyacenciasNuevas(antecesor.getListadoAdyacencias(), listadoVerticesUsados));
                antecesor = menor;
                //se busca la menor adyacencia y se inicia de nuevo el proceso
                contVertusados = contarVerticesUsados(listadoVerticesUsados);
            }
            //  JsfUtil.addErrorMessage("conVU" + contVertusados);
            // JsfUtil.addErrorMessage("ilvu" + listadoVerticesUsados.get(0).getVertice().getCodigo());
            // JsfUtil.addErrorMessage("flvu" + listadoVerticesUsados.get(listadoVerticesUsados.size() - 1).getVertice().getCodigo());
            /**
             * Ya deben estar los antecesores listos y enlazados, se deben
             * recorrer de atras hacia adelante hasta llenar la ruta empezando
             * en el nodo final
             */
            VerticeDijkstra destino = null;
            ruta.add(nodoFinal);
            int codigoAnt = nodoFinal.getCodigo();
            /**
             * ciclo donde se esta validando cuando no hay ruta
             */
            do {
                destino = obtenerVerticeAntecesorxCodigo(codigoAnt, listadoVerticesUsados);
                if (destino == null) {
                    break;
                } else if (destino.getVerticeAntecesor() == null) {
                    break;
                }
                codigoAnt = destino.getVerticeAntecesor().getCodigo();
                ruta.add(destino.getVerticeAntecesor());
                if (ruta.size() == listadoVerticesUsados.size()) {
                    break;

                }
            } while (codigoAnt != nodoInicio.getCodigo());
            //   JsfUtil.addErrorMessage("Iruta" + ruta.get(0).getCodigo());
            //   JsfUtil.addErrorMessage("Fruta" + ruta.get(ruta.size() - 1).getCodigo());
//              if (ruta.get(ruta.size() - 1) != listadoVerticesUsados.get(0).getVertice()) {
//            JsfUtil.addErrorMessage("no hay ruta");
//            List<Vertice> ruta2 = new ArrayList<>();
//            ruta2 = ruta;
//            ruta.clear();
//            ruta = ruta2;
//        }
            /**
             * se valida que tanto el final de la ruta como el inicio no sean
             * iguales
             */
            if (ruta.get(ruta.size() - 1) == ruta.get(0)) {
                JsfUtil.addErrorMessage("no hay ruta");
                ruta = new ArrayList<>();
                /**
                 * en el else if se valida que si el final de la ruta es
                 * diferente del inicio no hay camino
                 */
            } else if (ruta.get(ruta.size() - 1) != nodoInicio) {
                JsfUtil.addErrorMessage("no hay ruta");
                ruta = new ArrayList<>();
            }
//            return ruta;
        } else {
            JsfUtil.addErrorMessage("no hay ruta");
            ruta = new ArrayList<>();
        }
        return ruta;
    }

    /**
     * Metodo que permite obtener un vertice antecesor por medio del codigo del vertice
     * @param codigo es el que permite buscar el vertice antecesor por codigo
     * @param lista VerticeDijkstra me llama el vertice de la lista
     * @return VerticeDijkstra me retorna el vertice 
     */


    public VerticeDijkstra obtenerVerticeAntecesorxCodigo(int codigo, List<VerticeDijkstra> lista) {
        for (VerticeDijkstra vert : lista) {
            if (vert.getVertice().getCodigo() == codigo) {
                return vert;
            }
        }

        return null;
    }
    /**
     * Metodo que permite contar todos los vertices usados 
     * @param lista la que me almacena los vertices que estan siendo usados
     * @return los vertices que estan usados
     */
    
    public int contarVerticesUsados(List<VerticeDijkstra> lista) {
        int cont = 0;
        for (VerticeDijkstra vert : lista) {
            if (vert.isUsado()) {
                cont++;
            }
        }
        return cont;
    }
    
  
     /**
      * Metodo que permite otener las adyacencias nuevas 
      * @param vertice muestra las adyacencias de los vertices
      * @param listadoUsados se guardan los vertices que en el momento estan usados
      * @return va mostrar los vertices 
      */

    
        public VerticeDijkstra obtenerVerticeNoUsado(Vertice vertice, List<VerticeDijkstra> listadoUsados) {
        int cont = 0;
        VerticeDijkstra verticeD = new VerticeDijkstra();
        /**
         * ciclo que me permite saber si todos los vertices de un grado ya
         * fuaron usados parapoder saltar a otro grafo
         */

        for (int i = 0; i < listadoUsados.size(); i++) {
            if (vertice.getCodigo() == listadoUsados.get(i).getVertice().getCodigo()) {
                cont++;
            }
        }
        /**
         * se esta creando un inicio ara otro vertice
         */
        if (cont == 0) {
            verticeD.setVertice(vertice);
            verticeD.setPesoAcumulado(cont);
            verticeD.setUsado(true);
            verticeD.setVerticeAntecesor(null);
            return verticeD;
        }
        return null;
    }

        
    public List<VerticeDijkstra> obtenerAdyacenciasNuevas(List<VerticeDijkstra> listadoAdyacencias, List<VerticeDijkstra> listadoVerticesUsados) {
        List<VerticeDijkstra> nuevas = new ArrayList<>();
        for (VerticeDijkstra vert : listadoAdyacencias) {
            if (vert.obtenerVerticeDijkstraxCodigo(vert.getVertice().getCodigo(), listadoVerticesUsados) == null) {
                nuevas.add(vert);
            }
        }
        return nuevas;

    }
    /**
     * Metodo que permite saltar cuando encuentra un bloque en el grafo
     * @param vertices me permite adicionar los vertices
     * @param verticesdik me adiciona los vertices con el algoritmo de dijkstra
     * @return me retorna el nodo con menor peso donde realizo el salto
     */
    public VerticeDijkstra saltar(List<Vertice> vertices, List<VerticeDijkstra> verticesdik) {
        VerticeDijkstra menor = new VerticeDijkstra();
        for (Vertice verticesGrafo : vertices) {
            for (VerticeDijkstra verticesUsados : verticesdik) {
                if (verticesUsados.getVertice() == verticesGrafo && !verticesUsados.isUsado()) {
                    menor = verticesUsados;
                } else {

                    VerticeDijkstra inicio = new VerticeDijkstra();
                    inicio.setVertice(verticesGrafo);
                    inicio.setVerticeAntecesor(null);
                    inicio.setPesoAcumulado(0);
                    menor = inicio;
                }
            }

        }
        return menor;

    }

}
