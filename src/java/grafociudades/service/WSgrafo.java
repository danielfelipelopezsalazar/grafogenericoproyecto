/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.service;

import grafociudades.controlador.ControladorGrafo;
import grafociudades.controlador.Dijkstra;
import grafociudades.excepciones.GrafoExcepcion;
import grafociudades.modelo.Arista;
import grafociudades.modelo.Vertice;
import static java.awt.SystemColor.control;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *  Clase que representa el servicio web ambulapp 
 * @author Alejandro
 */
@WebService(serviceName = "WSgrafo")
@Stateless
public class WSgrafo {
    /**
     * atributo que permite inicilizar el ControladorGrafoSiembras 
     */

    @EJB
    private ControladorGrafo control;
    /**
     * This is a sample web service operation
     */
    
//    @WebMethod(operationName = "iniciarGrafo")
//    public String iniciarGrafo(){
//        control = new ControladorGrafo();
//        control.inicializar();
//        return "Grafo iniciado";  
//    }
    
    /**
     * retorna la lista de vertices del grafo
     * @return  ete me devuelve los verices a listar
     */
    @WebMethod(operationName = "listarVertices")
    public List<Vertice> listarVertices() {
        return control.getGrafoND().getVertices();
    }
    /**
     * Metodo que permite retornar la lista de vertices existentes
     * @return lista de vertices creados
     */

    @WebMethod(operationName = "listarAristas")
    public List<Arista> listarArista() {
        return control.getGrafoND().getAristas();
    }
 /**
  * Metodo que permite eliminar una arista del grafo
  * @param inicio encuentra el primer 
  * @param fin encuntra el ultimo nodo
  * @return elimina  
  */
    @WebMethod(operationName = "eliminarArista")
    public String eliminarArista(@WebParam(name="inicio") int inicio, @WebParam(name="fin") int fin) {
        control.getGrafoND().removerArista(inicio, fin);
        return "Arista eliminada";
    }
   /**
    * Metodo que permite eliminar vertices del grafo
    * @param id_vertice me busca por codigo el vertice
    * @return me rotorna el vertice elimanado
    */
    @WebMethod(operationName = "eliminarVertices")
    public String eliminarVertice(@WebParam(name="id_vertice") int id_vertice) {
        control.getGrafoND().removerVertice(id_vertice);
        return "Vertice eliminado";
    }
    
    /**
     * metodo que permite crear una arista
     * @param origen recive la arista de origen 
     * @param destino recive a donde se va realizar la conexion 
     * @return un mensaje de arista creada
     * @throws GrafoExcepcion me retorna mensaje de error
     */
    @WebMethod(operationName = "crearArista")
    public String crearArista(int origen, int destino) throws GrafoExcepcion{
        control.getGrafoND().verificarArista(origen, destino);
        control.getGrafoND().adicionarArista(new Arista(origen, destino, 1));
        return "arista creada";
    } 
    
    /**
     * metodo que permite mostrar arista
     * @return la lista de aristas que tiene el grafo
     */
    @WebMethod(operationName = "mostrarAristas")
        public List<Arista> mostrarArista( ) {
         return control.getGrafoND().getAristas();
    }
    
        /**
         * Metodo que permite buscar la ruta mas corta
         * @param codigoinicio codigo que me representa el vertice desde el cual se va inicializar la ruta
         * @param codigofinal  codigo que me represente el veetice final para la conexion
         * @return  me va retorna la ruta que estamos buscando
         */
     @WebMethod(operationName = "BuscarRutaCorta")
     
     
    public List<Vertice>BuscarRutaCorta (int codigoinicio,int codigofinal) {
     List<Vertice>ruta = new ArrayList<>();
         if (codigofinal != codigoinicio) {
             Dijkstra dijkstra = new Dijkstra(control.getGrafoND(), control.getGrafoND().obtenerVerticexCodigo(codigoinicio),control.getGrafoND().obtenerVerticexCodigo(codigofinal));
             ruta = dijkstra.calcularRutaMasCorta();
             
         }
        return ruta;
    }
    
    }




